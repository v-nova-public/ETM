include_directories ("${PROJECT_SOURCE_DIR}/app")

# Adds sources to the Solution Explorer
file (GLOB APP_INC "*.h")

add_executable (evca_encoder evca_encoder.c ${APP_INC})
add_executable (evca_decoder evca_decoder.c ${APP_INC})

include_directories(${CMAKE_CURRENT_BINARY_DIR})

# Properties->Linker->Input->Additional Dependencies
target_link_libraries (evca_encoder evc_encoder_lib)
target_link_libraries (evca_decoder evc_decoder_lib)

# Creates a folder "executables" and adds target
# project (ETM.vcproj) under it
set_property(TARGET evca_encoder PROPERTY FOLDER ETM)
set_property(TARGET evca_decoder PROPERTY FOLDER ETM)

if( MSVC )
    target_compile_definitions( evca_encoder PUBLIC _CRT_SECURE_NO_WARNINGS )
    target_compile_definitions( evca_decoder PUBLIC _CRT_SECURE_NO_WARNINGS )
endif()

if("${CMAKE_GENERATOR}" MATCHES "^Visual Studio")
  set_target_properties( evca_encoder PROPERTIES RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_SOURCE_DIR}/external_codecs/ETM")
  set_target_properties( evca_encoder PROPERTIES RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_SOURCE_DIR}/external_codecs/ETM")
  set_target_properties( evca_encoder PROPERTIES RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_SOURCE_DIR}/external_codecs/ETM")
  set_target_properties( evca_encoder PROPERTIES RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_SOURCE_DIR}/external_codecs/ETM")
else()
  set_target_properties( evca_encoder PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/external_codecs/ETM")
endif()

if("${CMAKE_GENERATOR}" MATCHES "^Visual Studio")
  set_target_properties( evca_decoder PROPERTIES RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_SOURCE_DIR}/external_codecs/ETM")
  set_target_properties( evca_decoder PROPERTIES RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_SOURCE_DIR}/external_codecs/ETM")
  set_target_properties( evca_decoder PROPERTIES RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_SOURCE_DIR}/external_codecs/ETM")
  set_target_properties( evca_decoder PROPERTIES RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_SOURCE_DIR}/external_codecs/ETM")
else()
  set_target_properties( evca_decoder PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/external_codecs/ETM")
endif()
